# source: https://docs.brew.sh/Node-for-Formula-Authors#example

require "language/node"

class Siastream < Formula
  desc "Next-gen cloud storage for your media"
  homepage "https://siastream.tech"
  url "https://registry.npmjs.org/siastream/-/siastream-1.0.4.tgz"
  version "1.0.4"
  sha256 "6bef91e77aa5873a47a868f46634c6da6c60f9ed7a2ae10b22e5f33f36c08ac3"

  depends_on "node"

  def install
    system "npm", "install", *Language::Node.std_npm_install_args(libexec)
    bin.install_symlink Dir["#{libexec}/bin/*"]
  end

  test do
    raise "Test not implemented."
  end
end
