#!/usr/bin/env bash
set -e

# Download latest runner
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64

# Set permissions
sudo chmod +x /usr/local/bin/gitlab-runner

# Read prepared variable
token=`cat ~/gitlab-registration-token.txt`

# Register runner
gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token $token --executor shell --description Mac-Mini --tag-list nebulous-macos

# Install and run the service
pushd ~ > /dev/null
gitlab-runner install
gitlab-runner start
popd