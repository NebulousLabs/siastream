# Setup Gitlab MacOS Runner

## Installation Summary

MacOS Gitlab runner should be installed in user mode using terminal opened
in MacOS GUI (i.e. ssh scripting can't be used).

This can be done remotely using ARD (Apple Remote Desktop = MacOS VNC server),
tunneling VNC connection via SSH and opening VNC stream on control machine
localhost.

Installation script is then run from terminal in remote MacOS GUI.

## Installation Procedure

### Enable ARD (Apple Remote Desktop)

- Connect to MacOS machine via ssh.
- Execute the following script on MacOS machine:

  `sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on -configure -allowAccessFor -allUsers -configure -restart -agent -privs -all`

The script starts ARD (which is a VNC server) at MacOS port 5900.

For security reasons, you should disable ARD when done (described later).

### Setup SSH Tunneling

- on your machine execute

  `ssh -N -L 5901:localhost:5900 <macos_user>@<ip_address_of_macos_box>`

This script creates SSH tunnel from MacOS ARD (VNC stream) on port 5900
to localhost port 5901.

### Connect via VNC

- Connect to `localhost:5901` via VNC client (viewer).
  E.g. Remmina VNC client can be used on Linux.

### Token Preparation

- In Gitlab repo where you want to use this runner, open `Settings > CI/CD`
- Expand `Runners`
- Find `Use the following registration token during setup`
- Copy the token via SSH or via MacOS GUI to the following file in MacOS home directory
  `~/gitlab-registration-token.txt` without leading or trailing
  spaces or newlines

### Execute Installation Script on MacOS

- Login to MacOS GUI using VNC viewer
- Download the installation script using curl or via browser to MacOS
- Open MacOS terminal
- Execute the script without sudo using `./setup-gitlab-macos-runner.sh`

  (using sudo would not install runner in user mode
  and the installation will be spoiled).

The script does:

- Downloads latest Gitlab runner executable
- Registers the runner using the prepared token with label `nebulous-macos`
- Installs the runner as a service in user mode
- Starts the runner service

### Verify Runner Registration and Connection

- In Gitlab repo where you want to use this runner, open `Settings > CI/CD`
- Expand `Runners`
- You should see the runner `Mac-Mini` active (green circle)
  under `Runners activated for this project`

### Disable ARD

- For security reasons, disable ARD (VNC server) using the following script:

  `sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -configure -access -off`
