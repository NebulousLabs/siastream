#!/usr/bin/env bash
set -e

## Install node via nvm

touch ~/.bash_profile
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
source ~/.bash_profile
nvm install --lts

## Set needed brew permissions (from plex user)

sudo chown -R $(whoami) $(brew --prefix)/*

sudo chown -R $(whoami) /usr/local/share/zsh
chmod u+w /usr/local/share/zsh
chmod u+w /usr/local/share/zsh/site-functions
chmod u+w /usr/local/var/homebrew/locks

## Install yarn

brew install yarn

## Install FUSE for Mac

brew tap homebrew/cask
brew cask install osxfuse
