import os from "os";
import execa from "execa"; // eslint-disable-line import/default

export async function unmount(mountpoint: string) {
  switch (os.platform()) {
    case "darwin":
      // use macos umount to forcefully unmount fuse from the mountpoint
      // -f for forceful removal
      // -v for verbose output in case of failure
      await execa.command(`umount -fv ${mountpoint}`);
      break;
    case "linux":
      // use linux fusermount to manually unmount fuse from the mountpoint
      await execa.command(`fusermount -u ${mountpoint}`);
      break;
    default:
      throw new Error(`Platform ${os.platform()} is not supported`);
  }
}
