import { BLOCKS_PER_MONTH, BYTES_IN_TERABYTE, toHastings } from "sia-typescript";
import { STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH } from "../constants";
import { getSiacoinExchangeRate } from "./currency";

// percentage of recommended wallet funds to allocate as allowance funds
const ALLOWANCE_FUNDS_ALLOCATION_PERCENTAGE = 75;

/**
 * Calculate expected allowance funds for given period compliant with SiaStream pricing model
 * @param allowancePeriod renter.settings.allowance.period
 * @param expectedStorage renter.settings.allowance.expectedstorage
 */
export async function calculateAllowanceFunds(allowancePeriod: number, expectedStorage: number) {
  const recommendedWalletFunds = await calculateRecommendedWalletFunds(allowancePeriod, expectedStorage);

  return ((BigInt(recommendedWalletFunds) * BigInt(ALLOWANCE_FUNDS_ALLOCATION_PERCENTAGE)) / BigInt(100)).toString();
}

/**
 * Calculate expected wallet funds for given period compliant with SiaStream pricing model
 * @param allowancePeriod renter.settings.allowance.period
 * @param expectedStorage renter.settings.allowance.expectedstorage
 */
export async function calculateRecommendedWalletFunds(allowancePeriod: number, expectedStorage: number) {
  /**
   * This is the Siacoin to USD exchange rate. At the time of writing it was around 0.00198723.
   * We need that value because SiaStream pricing model is based on the USD / TB / month and
   * we have to calculate the correct pricing.
   */
  const currencyExchangeRate = await getSiacoinExchangeRate();

  /**
   * This is allowance period which at the time of writing is almost 3 months and will most likely not change.
   * We need to know how many months the period encompasses since we will calculate the total allowance funds
   * based on multiplication of the number of months it is for (our pricing model is per month).
   */
  const periodInMonths = allowancePeriod / BLOCKS_PER_MONTH;

  /**
   * Calculate funds in hastings and assign it to the allowance diff so it will be picked up by the update request.
   */
  return toHastings(
    (STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH * expectedStorage * periodInMonths) /
      (currencyExchangeRate * BYTES_IN_TERABYTE)
  ).toString();
}

export function calculateMonthlySiaStreamPrice(expectedStorage: number) {
  return (expectedStorage * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH) / BYTES_IN_TERABYTE;
}

export function calculateInitialSiaStreamDeposit(expectedStorage: number) {
  return (expectedStorage * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH) / BYTES_IN_TERABYTE;
}
