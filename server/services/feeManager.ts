import { BYTES_IN_TERABYTE, BLOCKS_PER_MONTH, RenterGET } from "sia-typescript";
import { STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH } from "../constants";

const EXCHANGE_RATE_EXPONENT = 1e8; // we need that to multiply and divide exchange rate to use it as BigInt
const HASTINGS_IN_SIACOIN = BigInt("1000000000000000000000000"); // 1e24

/**
 * Function takes renter data and calculates current period spending in hastings.
 * In case there is also other traffic on the siad node, we allow setting a proportion to calculate
 * what was the part of the actual spending that we should treat as SiaStream spending.
 *
 * @param renter renter data from siad
 * @param proportion calculate proportion of the current spending
 * @returns current spending amount in hastings
 */
export function getCurrentSpending(renter: RenterGET, proportion = 1): bigint {
  const currentSpending =
    BigInt(renter.financialmetrics.contractfees) +
    BigInt(renter.financialmetrics.downloadspending) +
    BigInt(renter.financialmetrics.uploadspending) +
    BigInt(renter.financialmetrics.storagespending);

  return currentSpending * BigInt(proportion);
}

/**
 * Calculates siastream price for given period of time based on storage amount and current siacoin exchange rate.
 *
 * @param exchangeRate exchange rate of siacoin to USD
 * @param storageSize storage amount in bytes
 * @param period number of blocks that the current billing period consisted of
 * @returns siastream price in hastings
 */
export function calculateSiaStreamPrice(exchangeRate: number, storageSize: number, period: number): bigint {
  const storagePriceUSD =
    (storageSize * period * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH) / (BYTES_IN_TERABYTE * BLOCKS_PER_MONTH);

  return (
    (BigInt(Math.round(storagePriceUSD * EXCHANGE_RATE_EXPONENT)) * HASTINGS_IN_SIACOIN) /
    BigInt(Math.round(exchangeRate * EXCHANGE_RATE_EXPONENT))
  );
}

/**
 * Calculate SiaStream fee based on a price and current spending. We only charge up to our price for
 * the usage and if the spending was more than our price is, the fee will be zero.
 *
 * @param siaStreamPrice
 * @param currentSpending
 */
export function calculateCurrentSiaStreamFee(siaStreamPrice: bigint, currentSpending: bigint) {
  if (siaStreamPrice > currentSpending) {
    return siaStreamPrice - currentSpending;
  }

  return BigInt(0);
}
