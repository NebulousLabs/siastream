import fs from "fs";
import path from "path";
import expandTilde from "expand-tilde";
import * as yup from "yup";

yup.addMethod<yup.StringSchema>(yup.string, "validDir", function (message) {
  return this.test("valid-dir", message, function (v) {
    // null/undefined guard
    if (!v) {
      return true;
    }

    const z = expandTilde(v);
    // ensure not root
    if (z === "/") {
      return false;
    }
    return fs.existsSync(z) && fs.lstatSync(z).isDirectory();
  });
});

yup.addMethod<yup.StringSchema>(yup.string, "validBaseDir", function (message) {
  return this.test("valid-dir", message, function (v) {
    // null/undefined guard
    if (!v) {
      return true;
    }
    const z = path.dirname(expandTilde(v));
    return fs.existsSync(z) && fs.lstatSync(z).isDirectory();
  });
});

yup.addMethod<yup.StringSchema>(yup.string, "validSiaPath", function (message) {
  return this.test("valid-dir", message, function (v) {
    // null/undefined guard
    if (!v) {
      return true;
    }
    const isDir = !path.extname(v);
    return isDir;
  });
});

export const streamConfigSchema = yup.object().shape({
  fuseMountPath: yup.string().required().validBaseDir("not a valid fuse mount directory").required(),
  localPath: yup.string().validDir("not a valid local path directory").required(),
  siaStreamSiaPath: yup.string().validSiaPath("not a valid siapath directory").required(),
  removeMediaAfterUpload: yup.bool(),
});

export type StreamConfig = yup.InferType<typeof streamConfigSchema>;

export default yup;
