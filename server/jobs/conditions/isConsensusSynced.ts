import retry from "async-retry";
import { Logger } from "winston";
import { getSiaClient } from "../../siaClient";

/**
 * Check if consensus is already synced
 *
 * @param logger instance of winston logger
 */
export default async function isConsensusSynced(logger: Logger) {
  const SiaClient = getSiaClient(); // get instance of sia client
  const consensus = await retry(() => SiaClient.Consensus()); // get current consensus state

  if (!consensus.synced) {
    logger.silly("Consensus not yet synced, skipping");

    return false;
  }

  return true;
}
