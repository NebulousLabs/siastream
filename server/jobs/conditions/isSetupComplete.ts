import { Logger } from "winston";
import db from "../../db";

/**
 * Check if the setup is already complete
 *
 * @param logger instance of winston logger
 */
export default function isSetupComplete(logger: Logger) {
  const streamConfig = db.stream.getState(); // get local stream config

  if (!streamConfig.isComplete) {
    logger.silly("Onboarding not complete, skipping");

    return false;
  }

  return true;
}
