import retry from "async-retry";
import filesize from "filesize";
import { toHumanReadable } from "sia-typescript";
import db from "../db";
import winston from "../lib/winston";
import { getSiacoinExchangeRate } from "../services/currency";
import { getCurrentSpending, calculateSiaStreamPrice, calculateCurrentSiaStreamFee } from "../services/feeManager";
import { getSiaClient } from "../siaClient";
import { isConsensusSynced, isSetupComplete } from "./conditions";

const logger = winston.child({ label: "fee manager job" });

// Sia wallet address to receive SiaStream fees
const FEE_ADDRESS = "bd80c09a2325a3245d4839016f6067c50d57268e86ec86805a33f28a9eca09ac9137bc08c520";

export default async function feeManager() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  const SiaClient = getSiaClient();
  const renter = await retry(() => SiaClient.Renter()); // get current renter state
  const period = renter.settings.allowance.period;
  const streamConfig = db.stream.getState(); // get local stream config
  const consensus = await retry(() => SiaClient.Consensus()); // get current consensus height
  const expectedNextFeeHeight = renter.nextperiod - 1;
  const nextFeeHeight = streamConfig.nextFeeHeight ?? expectedNextFeeHeight;
  /**
   * This is the consensus height for which the fee has already been paid.
   * It initializes first as a current period height.
   */
  const paidFeeHeight = streamConfig.paidFeeHeight ?? renter.currentperiod;
  /**
   * This is the block length for which we want to charge a fee.
   * Initialy it will be (period + renew window) because paid fee height will equal to
   * current period and they will equal out. After we persist paid fee height, every
   * subsequent delta will equal to one period.
   */
  const contractLength = renter.settings.allowance.period + renter.settings.allowance.renewwindow;
  const feePeriodDelta = renter.currentperiod + contractLength - paidFeeHeight;

  /**
   * If consensus height is less than next fee height it means we have not yet
   * reached a point where we should process a fee thus we will skip and wait
   * for another loop.
   */
  if (consensus.height < nextFeeHeight) {
    // in case nextFeeHeight is not set in database, update it to correct value
    if (!streamConfig.nextFeeHeight) {
      db.partialSet(db.stream, { nextFeeHeight });
    }

    logger.silly("New fee block height not reached yet, skipping fee", {
      currentPeriod: renter.currentperiod,
      nextPeriod: renter.nextperiod,
      consensusHeight: consensus.height,
      nextFeeHeight,
    });

    return;
  }

  // get root directory size
  const rootDir = await retry(() => SiaClient.GetDir("/"));
  const rootDirSize = rootDir.directories[0].aggregatesize;

  // get siastream directory size
  const siaStreamDir = await retry(() => SiaClient.GetDir(streamConfig.siaStreamSiaPath));
  const siaStreamDirSize = siaStreamDir.directories[0].aggregatesize;

  // this is proportion of the data size in siastream directory relative to the overall data size on the node
  // if root dir size is zero, then siastream directory size is also zero so fall back to 1 (as in 100%)
  const siaStreamDirSizeProportion = rootDirSize ? siaStreamDirSize / rootDirSize : 1;

  // get siacoin usd exchange rate
  const currencyExchangeRate = await getSiacoinExchangeRate();

  // calculate SiaStream price for the given period of time
  const currentSiaStreamPrice = calculateSiaStreamPrice(currencyExchangeRate, siaStreamDirSize, feePeriodDelta);

  // calculate spending for the current period of time, in case user has other
  // data on the sia node, we will use proportions to calculate actual spending
  const currentSpending = getCurrentSpending(renter, siaStreamDirSizeProportion);

  // calculate what fee should SiaStream issue for this period based on
  // calculated price and current spending
  const currentSiaStreamFee = calculateCurrentSiaStreamFee(currentSiaStreamPrice, currentSpending);

  logger.info("SiaStream fee manager reached fee height", {
    currentStorage: filesize(siaStreamDirSize, { base: 10 }),
    currentSpending: toHumanReadable(currentSpending.toString()),
    currentSiaStreamPrice: toHumanReadable(currentSiaStreamPrice.toString()),
    currentSiaStreamFee: toHumanReadable(currentSiaStreamFee.toString()),
  });

  const payload = {
    appuid: "SiaStream",
    recurring: true,
    address: FEE_ADDRESS,
    amount: currentSiaStreamFee.toString(),
  };

  try {
    if (currentSiaStreamFee) {
      const { feeuid } = await SiaClient.makeRequest("/feemanager/add", payload, "POST");

      logger.info("Succesfully created new fee", { feeuid, payload });
    } else {
      logger.info("SiaStream fee for current period is zero, skipping fee");
    }

    db.partialSet(db.stream, {
      // next fee height should be either current fee height + period if we are
      // in the period that we are submitting the fee for or it should be set to
      // expected next fee height if we were submitting a fee for a past usage
      nextFeeHeight: nextFeeHeight === expectedNextFeeHeight ? nextFeeHeight + period : expectedNextFeeHeight,
      // persist paid fee height to be able to calculate the fee blocks delta
      paidFeeHeight: renter.currentperiod + feePeriodDelta,
    });
  } catch (error) {
    logger.error("Failed to add fee", { error: error.response ? error.response.body.message : error.message, payload });
  }
}
