import dayjs from "dayjs";
import isToday from "dayjs/plugin/isToday";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete, isConsensusSynced } from "./conditions";
import createRenterBackup from "./services/createRenterBackup";

dayjs.extend(isToday);

const logger = winston.child({ label: "renter backup job" });

export default async function renterBackup() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  // check if there has been a backup already today
  const SiaClient = getSiaClient();
  const backups = await SiaClient.RenterBackups();
  const todaysBackup = backups.backups.find((backup) => dayjs(backup.creationdate * 1000).isToday());

  if (todaysBackup) {
    logger.silly(`There was already a backup today "${todaysBackup.name}", no need to backup again, skipping`);

    return;
  }

  const streamConfig = db.stream.getState();

  if (streamConfig.removeMediaAfterUpload) {
    logger.silly(
      "streamConfig.removeMediaAfterUpload setting is enabled so creating a backup is handled by delete uploaded files job, skipping"
    );

    return;
  }

  createRenterBackup(logger);
}
