import retry from "async-retry";
import { updatedDiff } from "deep-object-diff";
import filesize from "filesize";
import { isEmpty } from "lodash";
import { toHumanReadable } from "sia-typescript";
import db from "../db";
import winston from "../lib/winston";
import { calculateAllowanceFunds } from "../services/finances";
import { getSiaClient } from "../siaClient";
import { isSetupComplete, isAutomaticAllowance, isConsensusSynced } from "./conditions";

const logger = winston.child({ label: "allowance update job" });

export default async function updateAllowanceSettings() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if the SiaStream is configured to automatically manage allowance
  if (!isAutomaticAllowance(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  const SiaClient = getSiaClient();
  const local = db.allowance.getState(); // get local renter allowance settings
  const renter = await retry(() => SiaClient.Renter()); // get node renter settings
  const allowanceDiff = updatedDiff(renter.settings.allowance, local) as any;

  // check if local allowance setting matches siad setting
  if (isEmpty(allowanceDiff)) {
    logger.silly("Local allowance settings match siad settings, skipping");

    return;
  }

  logger.debug(`Detected allowance updates: ${JSON.stringify(allowanceDiff)}`);

  if (allowanceDiff.expectedstorage && local.expectedstorage) {
    allowanceDiff.funds = await calculateAllowanceFunds(
      renter.settings.allowance.period,
      allowanceDiff.expectedstorage
    );

    const humanReadableRenterStorage = filesize(renter.settings.allowance.expectedstorage, { base: 10 });
    const humanReadableLocalStorage = filesize(local.expectedstorage, { base: 10 });
    const humanReadableFunds = toHumanReadable(allowanceDiff.funds);

    logger.debug(
      `Expected storage changed from ${humanReadableRenterStorage} to ${humanReadableLocalStorage} so funds get adjucted to ${humanReadableFunds}`
    );
  }

  try {
    await SiaClient.SetAllowance(local as any);

    logger.info(`Allowance updated`, allowanceDiff);
  } catch (error) {
    logger.error("Allowance update failure, aborting", { error: error.message });

    return;
  }

  logger.debug("Allowance updated successfully");
}
