import logger from "../lib/winston";

export const asyncMiddleware = (fn) => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch((err) => {
    if (err.statusCode) {
      logger.error(`${req.path}: ${err.message}`, { label: "Siad API" });

      return res.status(err.statusCode).send(err.error);
    }

    if (err.message) {
      logger.error(`request error: ${err.message}`);
      return res.status(500).end();
    }

    next(err);
  });
