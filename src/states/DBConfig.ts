import axios from "axios";
import { Allowance } from "sia-typescript";
import { StreamConfig } from "../../server/db";

const createSetCallback = <T>(configName: string, partialConfig: Partial<T>) => {
  return axios.post(`/api/config/${configName}`, partialConfig).then(({ data }) => data);
};

export const useDBConfig = () => {
  const setStreamConfig = (sc: Partial<StreamConfig>) => createSetCallback("stream", sc);
  const setRenterConfig = (rc: Partial<Allowance>) => createSetCallback("allowance", rc);

  return {
    setStreamConfig,
    setRenterConfig,
  };
};
