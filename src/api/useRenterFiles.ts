import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /renter/files endpoint react hook for renter files data fetching
 * https://sia.tech/docs/#renter-filess-get
 */
export default function useRenterFiles(config?: ConfigInterface) {
  return useSWR<any>("/siad/renter/files", fetcher, config);
}
