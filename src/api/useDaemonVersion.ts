import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /daemon/version endpoint react hook for daemon version data fetching
 * https://sia.tech/docs/#daemon-version-get
 */
export default function useDaemonVersion(config?: ConfigInterface) {
  return useSWR<any>("/siad/daemon/version", fetcher, config);
}
