import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /renter/recoveryscan endpoint react hook for renter recovery scan data fetching
 * https://sia.tech/docs/#renter-recoveryscan-get
 */
export default function useRenterRecoveryScan(config?: ConfigInterface) {
  return useSWR<any>("/siad/renter/recoveryscan", fetcher, config);
}
