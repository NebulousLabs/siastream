import { WalletGET } from "sia-typescript";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /wallet endpoint react hook for wallet data fetching
 * https://sia.tech/docs/#wallet-get
 */
export default function useWallet(config?: ConfigInterface) {
  return useSWR<WalletGET>("/siad/wallet", fetcher, config);
}
