import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /wallet/seeds endpoint react hook for wallet seeds data fetching
 * https://sia.tech/docs/#wallet-seeds-get
 */
export default function useWalletSeeds(config?: ConfigInterface) {
  return useSWR<any>("/siad/wallet/seeds", fetcher, config);
}
