import { sumBy } from "lodash";
import { useMemo } from "react";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

interface Pricing {
  usdRate: number | null;
  targetPriceInSC: number | null;
}

interface SiastatsStoragePriceEntry {
  date: string;
  price: number;
  sfperfees: number;
  contractformation?: number;
  newcontractformation?: number;
  usd?: number;
}

interface CoingeckoExchangeRate {
  siacoin: {
    usd: number;
  };
}

/**
 * React hook for fetching pricing info wrapped in SWR
 */
export default function usePricing(config?: ConfigInterface) {
  const { data: exchangeRate } = useSWR<CoingeckoExchangeRate>(
    "https://api.coingecko.com/api/v3/simple/price?ids=siacoin&vs_currencies=usd",
    fetcher,
    config
  );
  const { data: allowanceRate } = useSWR<SiastatsStoragePriceEntry[]>(
    "https://siastats.info/dbs/storagepricesdb.json",
    fetcher,
    config
  );

  return useMemo<Pricing>(() => {
    if (!exchangeRate) return { usdRate: null, targetPriceInSC: null };

    const usdRate = exchangeRate.siacoin.usd;

    if (!allowanceRate) return { usdRate, targetPriceInSC: null };

    // rolling 7 day avg price of a TB stored a 3x redundancy on Sia.
    const allowanceRateLast7Days = allowanceRate.filter(({ usd }) => usd).slice(-7);
    const allowanceRateLast7DaysAvarage = sumBy(allowanceRateLast7Days, "usd") / allowanceRateLast7Days.length;
    const targetPriceInSC = Math.round(allowanceRateLast7DaysAvarage / usdRate);

    return { usdRate, targetPriceInSC };
  }, [exchangeRate, allowanceRate]);
}
