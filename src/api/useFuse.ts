import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /fuse endpoint react hook for fuse data fetching
 * https://sia.tech/docs/#renter-fuse-get
 */
export default function useFuse(config?: ConfigInterface) {
  return useSWR<any>("/siad/fuse", fetcher, config);
}
