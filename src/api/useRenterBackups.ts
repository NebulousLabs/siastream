import { RenterBackups } from "sia-typescript";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /renter/backups endpoint react hook for renter backups data fetching
 * https://sia.tech/docs/#renter-backups-get
 */
export default function useRenterBackups(config?: ConfigInterface) {
  return useSWR<RenterBackups>("/siad/renter/backups", fetcher, config);
}
