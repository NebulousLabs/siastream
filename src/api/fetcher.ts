import axios from "axios";

/**
 * SWR data fetcher util without error handling which will be handled by SWR library
 * Error handling should be handled by the consumer https://github.com/axios/axios#handling-errors
 */
export default async function fetcher<T>(url: string) {
  const response = await axios.get<T>(url);

  return response.data;
}
