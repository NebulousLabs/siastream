/** @jsx jsx */
import { css, Global } from "@emotion/core";
import { ThemeProvider as MUIThemeProvider } from "@material-ui/core/styles";
import { SnackbarProvider } from "notistack";
import React, { Suspense } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { jsx, Styled, ThemeProvider } from "theme-ui";
import useSiadMonitor from "./hooks/useSiadMonitor";
import Context from "./states/Context";
import { base } from "./theme";
import muiTheme, { useSnackbarStyles } from "./theme/muiTheme";

const Main = React.lazy(() => import("./views/Main"));
const Setup = React.lazy(() => import("./views/Setup"));

function View() {
  useSiadMonitor();

  return (
    <Suspense fallback={null}>
      <Switch>
        <Route exact path="/">
          <Main />
        </Route>
        <Route path="/setup">
          <Setup />
        </Route>
      </Switch>
    </Suspense>
  );
}

function StyledSnackbar({ children }: { children: React.ReactNode }) {
  const classes = useSnackbarStyles();

  return <SnackbarProvider classes={classes}>{children}</SnackbarProvider>;
}

export default function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={base}>
        <MUIThemeProvider theme={muiTheme}>
          <Context>
            <StyledSnackbar>
              <Styled.root>
                <Global
                  styles={(theme) => {
                    return css`
                      body {
                        background: ${theme.colors.background};
                      }
                    `;
                  }}
                />
                <View />
              </Styled.root>
            </StyledSnackbar>
          </Context>
        </MUIThemeProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}
