import transakSDK from "@transak/transak-sdk";

const isProductionEnvironment = process.env.NODE_ENV !== "development";

/**
 * API keys were generated automatically during signup process.
 */
const TRANSAK_API_KEY_STAGING = "aee64e45-1789-425f-a730-d3501c487bdc";
const TRANSAK_API_KEY_PRODUCTION = "9bebea85-d3b6-40d9-a7c9-5b75b141e18f";

/**
 * Transak SDK documentation: https://github.com/Transak/transak-sdk
 * API query params list: https://integrate.transak.com/Query-Parameters-9ec523df3b874ec58cef4fa3a906f238
 */

const settings = {
  /**
   * Transak API key. Use staging key during development and production key on production builds.
   */
  apiKey: isProductionEnvironment ? TRANSAK_API_KEY_PRODUCTION : TRANSAK_API_KEY_STAGING,

  /**
   * Environment name. Accepted values are "STAGING" or "PRODUCTION".
   */
  environment: isProductionEnvironment ? "PRODUCTION" : "STAGING",

  /**
   * The default cryptocurrency you would prefer the customer to purchase.
   * If you pass a defaultCryptoCurrency, the currency will be selected by
   * default, but the customer will still be able to select another cryptocurrency.
   */
  defaultCryptoCurrency: "SC",

  /**
   * The code of the fiat currency you want the customer to buy cryptocurrency with.
   */
  fiatCurrency: "USD",

  /**
   * Domain name. The default value is window.location.origin. SDK requires this field
   * to communicate with transak system.
   */
  hostURL: window.location.origin,

  /**
   * The theme color code for the widget main color. It is used for buttons, links
   * and highlighted text. Only hexadecimal codes are accepted.
   */
  themeColor: "#000000",

  /**
   * Height of the widget iFrame.
   */
  widgetHeight: "700px",

  /**
   * Width of the widget iFrame.
   */
  widgetWidth: "500px",
};

type Options = {
  onSuccess(data): void;
};

export function openTransak(walletAddress: string, options?: Partial<Options>) {
  const transak = new transakSDK({ ...settings, walletAddress });

  transak.init();

  // This will trigger when the user marks payment is made.
  transak.on(transak.EVENTS.TRANSAK_ORDER_SUCCESSFUL, (data) => {
    if (options.onSuccess) {
      options.onSuccess(data);
    }

    transak.close();
  });
}
