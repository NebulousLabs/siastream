import ms from "ms";

/**
 * This function accepts a promise and returns new promise that:
 * - if the initial promise resolved or rejects, new promise will also resolve or reject
 * - if the initial promise doesn't resolve or reject in the timeout time, it will resolve with success
 *
 * This is very handy for promises that we know that if they didn't immediately resolve or reject, we can
 * assume that they will run in background and resolve with success at some point in the future.
 */
export default function assumeSuccessWhenNoImmediateResponse(promise: Promise<any>, timeout: number = ms("2 seconds")) {
  const timer = new Promise((resolve) => setTimeout(resolve, timeout));

  return Promise.race([promise, timer]);
}
