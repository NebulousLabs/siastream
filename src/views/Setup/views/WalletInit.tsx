import { Card, CardHeader, CardActionArea, CardContent, Typography, Grid, Box } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import StepTransitionAnimation from "../components/StepTransitionAnimation";
import useCurrentStepInit from "../hooks/useCurrentStepInit";

const currentStep = {
  help: {
    text: "What is a wallet?",
    href: "https://support.siastream.tech/article/xcqafpvufo-siastream-wallet",
  },
};

export default function WalletInit() {
  const history = useHistory();
  const handleWalletRestore = () => history.push("/setup/wallet/restore");
  const handleWalletInit = () => history.push("/setup/wallet/init/password");
  useCurrentStepInit(currentStep);

  return (
    <StepTransitionAnimation>
      <Grid container spacing={4}>
        <Grid item xs={6}>
          <Card variant="outlined">
            <CardActionArea onClick={handleWalletInit}>
              <CardHeader title="New Wallet" titleTypographyProps={{ variant: "h2", align: "center" }} />
              <CardContent>
                <Box minHeight={48}>
                  <Typography align="center">I want to create a new wallet</Typography>
                </Box>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card variant="outlined">
            <CardActionArea onClick={handleWalletRestore}>
              <CardHeader title="Load Wallet" titleTypographyProps={{ variant: "h2", align: "center" }} />
              <CardContent>
                <Box minHeight={48}>
                  <Typography align="center">I have a seed phrase to restore already created wallet</Typography>
                </Box>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>

      <Navigation justifyContent="flex-start">
        <BackButton />
      </Navigation>
    </StepTransitionAnimation>
  );
}
