import { CardHeader, Card, CardContent, Typography, Box, TextField, Link, InputAdornment } from "@material-ui/core";
import { useFormik } from "formik";
import React from "react";
import { useHistory } from "react-router-dom";
import { BYTES_IN_TERABYTE } from "sia-typescript";
import * as Yup from "yup";
import { useOS, useSiaStreamSettings } from "../../../api";
import { useDBConfig } from "../../../states/DBConfig";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function MediaSettings() {
  const history = useHistory();
  const { data: os } = useOS({ suspense: true });
  const { data: streamConfig, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", { suspense: true });
  const { data: allowanceConfig, mutate: mutateAllowanceConfig } = useSiaStreamSettings("allowance", {
    suspense: true,
  });
  const { setStreamConfig, setRenterConfig } = useDBConfig();
  const formik = useFormik({
    initialValues: {
      mediaRootDirectory: streamConfig.localPath,
      fuseMountPath: streamConfig.fuseMountPath,
      estimateSize: allowanceConfig.expectedstorage / BYTES_IN_TERABYTE,
    },
    validationSchema: Yup.object({
      mediaRootDirectory: Yup.string().required("Media root directory cannot be empty"),
      fuseMountPath: Yup.string().required("FUSE mount path cannot be empty"),
      estimateSize: Yup.number().min(1).max(20).required("Estimated media library size cannot be empty"),
    }),
    onSubmit: async ({ mediaRootDirectory, fuseMountPath, estimateSize }, { setSubmitting }) => {
      await mutateStreamConfig(setStreamConfig({ localPath: mediaRootDirectory, fuseMountPath }));
      await mutateAllowanceConfig(setRenterConfig({ expectedstorage: estimateSize * BYTES_IN_TERABYTE }));

      setSubmitting(false);

      history.push("/setup/complete");
    },
  });

  return (
    <StepTransitionAnimation>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <CardHeader title="Media Paths Configuration" titleTypographyProps={{ variant: "h2" }} />
          <CardContent>
            <Box mb={6}>
              <Typography variant="h3" paragraph gutterBottom>
                Media Root Directory
              </Typography>

              <Typography paragraph>
                SiaStream will automatically monitor this path and upload its contents to the Sia network. Choose the
                existing directory where your media is stored.
              </Typography>

              <Box mb={3}>
                <TextField
                  name="mediaRootDirectory"
                  label="Media Root Directory"
                  fullWidth
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.mediaRootDirectory && Boolean(formik.errors.mediaRootDirectory)}
                  helperText={formik.touched.mediaRootDirectory && formik.errors.mediaRootDirectory}
                  value={formik.values.mediaRootDirectory}
                />
              </Box>

              <Box mb={3}>
                <TextField
                  name="estimateSize"
                  label="Estimated Media Library Size"
                  fullWidth
                  InputProps={{
                    endAdornment: <InputAdornment position="end">TB</InputAdornment>,
                  }}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.estimateSize && Boolean(formik.errors.estimateSize)}
                  helperText={formik.touched.estimateSize && formik.errors.estimateSize}
                  value={formik.values.estimateSize}
                />
              </Box>
            </Box>

            <Box>
              <Box display="flex" justifyContent="space-between" alignItems="center" mb={3}>
                <Typography variant="h3">FUSE Mount Path *</Typography>

                <Link
                  href="https://support.siastream.tech/article/p4kecgw0ab-siastream-fuse"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  What is FUSE?
                </Link>
              </Box>

              <Typography paragraph>
                This is where SiaStream will mount the virtual drive with your uploaded media. You will add this path to
                Plex and other media apps to stream from.
              </Typography>

              <Box mb={3}>
                <TextField
                  name="fuseMountPath"
                  label="FUSE Mount Path"
                  fullWidth
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.fuseMountPath && Boolean(formik.errors.fuseMountPath)}
                  helperText={formik.touched.fuseMountPath && formik.errors.fuseMountPath}
                  value={formik.values.fuseMountPath}
                />
              </Box>

              {os.platform === "darwin" && (
                <Typography variant="caption">
                  * MacOS requires{" "}
                  <Link href="https://osxfuse.github.io" target="_blank" rel="noopener noreferrer">
                    FUSE
                  </Link>{" "}
                  installed
                </Typography>
              )}

              {os.platform === "linux" && (
                <Typography variant="caption">
                  * Linux requires user_allow_other enabled in /etc/fuse.conf when running as non-root user
                </Typography>
              )}
            </Box>
          </CardContent>
        </Card>

        <Navigation>
          <BackButton />

          <NextButton disabled={formik.isSubmitting} />
        </Navigation>
      </form>
    </StepTransitionAnimation>
  );
}
