import { Card, CardHeader, CardContent, Typography, Box, FormControlLabel, Checkbox } from "@material-ui/core";
import { motion } from "framer-motion";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";
import useCurrentStepInit from "../hooks/useCurrentStepInit";

const currentStep = {
  help: {
    text: "What is Seed?",
    href: "https://support.siastream.tech/article/wy1c7jv279-siastream-seed",
  },
};

const paragraphs = [
  "Next, you will generate a new seed, which is a string of words that represent your private keys. It is imperative to keep your seed safe! We recommend that you write it down and store multiple copies.",
  "Your seed is extremely powerful. In addition to restoring your wallet, your seed recovers all your uploaded media.",
  "There is no “forgot seed” feature – if you lose it, it is gone forever.",
];

export default function WalletInitSeedIntro() {
  const history = useHistory();
  const [current, setCurrent] = useState(0); // current active paragraph
  const [checked, setChecked] = useState([]); // list of checked paragraphs
  const complete = checked.length === paragraphs.length;
  const handleNext = () => history.push("/setup/wallet/init/seed");
  const handleCheck = (paragraph: number) => {
    if (current === paragraph) {
      setChecked([...checked, paragraph]);
      setCurrent(current + 1);
    }
  };

  useCurrentStepInit(currentStep);

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="Wallet Seed: The Key To Your Kingdom" titleTypographyProps={{ variant: "h2" }} />
        <CardContent>
          {paragraphs.map((paragraph, index) => (
            <motion.div key={index} animate={{ opacity: current === index ? 1 : 0.3 }}>
              <Box mb={index < paragraphs.length - 1 ? 4 : 0}>
                <Typography>{paragraph}</Typography>

                <FormControlLabel
                  control={
                    <Checkbox color="primary" checked={checked.includes(index)} onChange={() => handleCheck(index)} />
                  }
                  label="I have read and understand the above"
                />
              </Box>
            </motion.div>
          ))}
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <NextButton disabled={!complete} onClick={handleNext} />
      </Navigation>
    </StepTransitionAnimation>
  );
}
