import { Card, CardHeader, CardContent, Typography, Button } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function WalletExisting() {
  const history = useHistory();
  const handleNext = () => history.push("/setup/wallet/existing/unlock");
  const handleNew = () => history.push("/setup/wallet/init");

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="We found an existing Sia instance" titleTypographyProps={{ variant: "h2" }} />
        <CardContent>
          <Typography>Sia is already installed on your device. Would you like to use your existing wallet?</Typography>
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <Button variant="outlined" onClick={handleNew}>
          Set up new wallet
        </Button>

        <NextButton onClick={handleNext}>Use existing wallet</NextButton>
      </Navigation>
    </StepTransitionAnimation>
  );
}
