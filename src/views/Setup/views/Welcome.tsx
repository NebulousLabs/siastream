import { Card, CardHeader, CardContent, Typography, Link, Box } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import useWallet from "../../../api/useWallet";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function Welcome() {
  const { data: wallet } = useWallet({ suspense: true });
  const history = useHistory();
  const handleNext = () => {
    /**
     * Depending on whether there is already an encrypted wallet or not we will
     * redirect to different views. If there is no wallet yet, we will give options
     * to either create a new one or load from seed. If there is a wallet already, we
     * will encourage user to use it (but give an option to create new one or load from seed).
     */
    const step = wallet.encrypted ? "/setup/wallet/existing" : "/setup/wallet/init";

    history.push(step);
  };

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="Welcome to SiaStream!" titleTypographyProps={{ variant: "h2" }} />
        <CardContent>
          <Typography paragraph>The perfect home for your media.</Typography>

          <Typography paragraph>
            Enjoy amazingly cheap storage for your media files, completely under your control. SiaStream uses next-gen
            cloud storage technology to provide fast streaming at the lowest costs.
          </Typography>

          <Typography paragraph>
            We are currently setting up Sia, the decentralized cloud storage network that powers SiaStream. This
            one-time blockchain sync process can take several hours to complete on an SSD or a couple days on an HDD.
          </Typography>

          <Typography paragraph>In the meantime, let's configure SiaStream! We promise it's worth the wait.</Typography>

          <Box fontWeight="fontWeightBold">
            <Link
              target="_blank"
              rel="noopener noreferrer"
              href="https://support.siastream.tech/category/nk88rmpgn7-how-sia-works"
            >
              Learn More
            </Link>
          </Box>
        </CardContent>
      </Card>

      <Navigation justifyContent="flex-end">
        <NextButton onClick={handleNext} />
      </Navigation>
    </StepTransitionAnimation>
  );
}
