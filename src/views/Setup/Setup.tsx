import { Box, Container, CssBaseline, Grid, Link, Tooltip, IconButton } from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { Receipt, HelpOutline } from "@material-ui/icons";
import { AnimatePresence, motion } from "framer-motion";
import React, { Suspense, useEffect, useContext } from "react";
import { Switch, Route, Redirect, useRouteMatch } from "react-router-dom";
import LogDialog from "../../components/LogDialog";
import SiaCoin from "../../components/SiaCoin";
import SiadStatusChip from "../../components/SiadStatusChip";
import { AnimFadeIn } from "../../components/Transitions";
import { DispatchContext, StateContext } from "../../states/Context";
import Complete from "./views/Complete";
import MediaSettings from "./views/MediaSettings";
import WalletExisting from "./views/WalletExisting";
import WalletExistingUnlock from "./views/WalletExistingUnlock";
import WalletInit from "./views/WalletInit";
import WalletInitPassword from "./views/WalletInitPassword";
import WalletInitSeed from "./views/WalletInitSeed";
import WalletInitSeedIntro from "./views/WalletInitSeedIntro";
import WalletRestore from "./views/WalletRestore";
import Welcome from "./views/Welcome";

const AppContainer = styled(Container)(({ theme }) => ({
  paddingTop: theme.spacing(4),
  maxWidth: 720,
}));

/**
 * React hook that initializes setup flow. It dispatches
 * "SETUP/INITIALIZE" event if user lands on /setup route.
 * This allows us to redirect user to starting screen if he
 * lands on any subroute (ie due to browser refresh). Setup
 * relies on context state so we need to ensure it is completed
 * from the beggining to the end in one sesssion.
 */
const useSetupInit = () => {
  const { path, isExact } = useRouteMatch();
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  const initialized = state.setup.initialized;

  useEffect(() => {
    if (path === "/setup" && isExact && !initialized) {
      dispatch({ type: "SETUP/INITIALIZE" });
    }
  }, [dispatch, path, isExact, initialized]);
};

export default function Setup() {
  const { path, isExact } = useRouteMatch();
  const state = useContext(StateContext);
  const dispatch = useContext(DispatchContext);

  useSetupInit();

  // Redirect user to /setup if current route is a subroute
  // but setup was not started at initial route.
  if (!state.setup.initialized && !(path === "/setup" && isExact)) {
    return <Redirect to={"/setup"} />;
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <LogDialog />
      <AppContainer>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Box display="flex" justifyContent="space-between" alignItems="center">
              <SiaCoin />

              <Box display="flex" alignItems="center">
                {state.setup.currentStep.help && (
                  <Box mr={2} component={motion.div} {...AnimFadeIn}>
                    <Link href={state.setup.currentStep.help.href} target="_blank" rel="noopener noreferrer">
                      {state.setup.currentStep.help.text}
                    </Link>
                  </Box>
                )}

                <Suspense fallback={null}>
                  <Box mr={2}>
                    <SiadStatusChip />
                  </Box>
                </Suspense>

                <Tooltip title={"Show log"} aria-label="show log">
                  <IconButton onClick={() => dispatch({ type: "LOG/OPEN" })}>
                    <Receipt />
                  </IconButton>
                </Tooltip>

                <Tooltip title={"Support site"} aria-label="support">
                  <IconButton href="https://support.siastream.tech" target="_blank">
                    <HelpOutline />
                  </IconButton>
                </Tooltip>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <AnimatePresence>
              <Suspense fallback={null}>
                <Switch>
                  <Route path={"/setup"} exact>
                    <Welcome />
                  </Route>
                  <Route path={"/setup/wallet/init"} exact>
                    <WalletInit />
                  </Route>
                  <Route path={"/setup/wallet/init/password"} exact>
                    <WalletInitPassword />
                  </Route>
                  <Route path={"/setup/wallet/init/seedintro"} exact>
                    <WalletInitSeedIntro />
                  </Route>
                  <Route path={"/setup/wallet/init/seed"} exact>
                    <WalletInitSeed />
                  </Route>
                  <Route path={"/setup/wallet/existing"} exact>
                    <WalletExisting />
                  </Route>
                  <Route path={"/setup/wallet/existing/unlock"} exact>
                    <WalletExistingUnlock />
                  </Route>
                  <Route path={"/setup/wallet/restore"} exact>
                    <WalletRestore />
                  </Route>
                  <Route path={"/setup/media"}>
                    <MediaSettings />
                  </Route>
                  <Route path={"/setup/complete"}>
                    <Complete />
                  </Route>
                </Switch>
              </Suspense>
            </AnimatePresence>
          </Grid>
        </Grid>
      </AppContainer>
    </React.Fragment>
  );
}
