import { motion } from "framer-motion";
import React, { ReactNode } from "react";

interface Props {
  children?: ReactNode | ReactNode[];
}

const transition = {
  initial: { opacity: 0, x: -50 },
  animate: { opacity: 1, x: 0 },
  exit: { opacity: 0, x: -50 },
};

export default function StepTransitionAnimation({ children }: Props) {
  return <motion.div {...transition}>{children}</motion.div>;
}
