import { Button, Dialog, Box, IconButton, DialogContent, DialogTitle, Divider, Typography } from "@material-ui/core";
import { FileCopy } from "@material-ui/icons";
import { useSnackbar } from "notistack";
import QRCode from "qrcode.react";
import React, { useEffect, useContext, useState } from "react";
import { openTransak } from "../services/transak";
import { DispatchContext, StateContext } from "../states/Context";
import { SiaGenerateAddress } from "../util/siafetch";

export function DepositAddressModal() {
  const { depositDialogOpened } = useContext(StateContext);
  const dispatch = useContext(DispatchContext);
  const [walletAddress, setWalletAddress] = useState("");
  const { enqueueSnackbar } = useSnackbar();
  const closeDialog = () => dispatch({ type: "DEPOSIT_DIALOG/CLOSE" });
  const handleCopyToClipboard = () => {
    closeDialog();
    navigator.clipboard.writeText(walletAddress);
    enqueueSnackbar("Wallet address copied to clipboard", { preventDuplicate: true });
  };
  const handleTransak = () => {
    closeDialog();
    openTransak(walletAddress, {
      onSuccess: () => {
        enqueueSnackbar(
          "Your deposit is on its way to your wallet. It can take anywhere between 10 minutes and 3 business days depending on the payment method.",
          { variant: "success" }
        );
      },
    });
  };

  // generate wallet address when deposit dialog is opened
  useEffect(() => {
    if (depositDialogOpened) {
      SiaGenerateAddress().then(
        ({ address }) => {
          setWalletAddress(address);
        },
        (error) => {
          const message = error.response ? error.response.data.message : error.message;

          enqueueSnackbar(`Sia failed to generate a new wallet address: ${message}`, { variant: "error" });
        }
      );
    }
  }, [depositDialogOpened, enqueueSnackbar]);

  return (
    <Dialog
      fullWidth
      maxWidth="sm"
      open={depositDialogOpened && Boolean(walletAddress)}
      onClose={closeDialog}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Deposit Siacoin</DialogTitle>
      <DialogContent>
        {walletAddress && (
          <Box display="flex">
            <QRCode value={walletAddress} size={220} includeMargin={true} />

            <Box textAlign="center" ml={3} display="flex" flexDirection="column" justifyContent="space-around">
              <Box>
                <Typography gutterBottom>Wallet address</Typography>

                <Box display="flex">
                  <Box fontFamily="Monospace" fontSize="14px" style={{ wordBreak: "break-all" }}>
                    {walletAddress}
                  </Box>

                  <IconButton onClick={handleCopyToClipboard}>
                    <FileCopy />
                  </IconButton>
                </Box>
              </Box>

              <Divider />

              <Button onClick={handleTransak} variant="contained">
                Buy with <img src="/images/transak.png" height="15px" alt="Transak" style={{ margin: "0 5px" }} />{" "}
                Transak
              </Button>
            </Box>
          </Box>
        )}
      </DialogContent>
    </Dialog>
  );
}
