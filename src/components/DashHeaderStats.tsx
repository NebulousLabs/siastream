/** @jsx jsx */
import { Tooltip } from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import filesize from "filesize";
import { omit } from "lodash";
import ms from "ms";
import { FileInfo } from "sia-typescript";
import { Box, jsx, Styled } from "theme-ui";
import useRenterContracts from "../api/useRenterContracts";
import useRenterFiles from "../api/useRenterFiles";
import { CardBody, CardFluid } from "../components/Card";
import { formatNumber } from "../util/utils";

const DashCard = (p) => (
  <CardFluid>
    <CardBody
      sx={{
        position: "relative",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        textAlign: "center",
        p: 36,
        height: "100%",
      }}
      {...omit(p, ["onClick"])}
    ></CardBody>
  </CardFluid>
);

const DashCardTitle = styled("div")({
  fontWeight: "bold",
  letterSpacing: 1.2,
  textTransform: "uppercase",
});

export const DashHeaderStats = () => {
  const { data: contracts } = useRenterContracts({ refreshInterval: ms("5 seconds") });
  const { data: files } = useRenterFiles({ refreshInterval: ms("5 seconds") });

  const filesStored = files?.files ?? [];
  const filesStoredCount = filesStored.length;
  const filesStoredSize = filesStored.reduce((acc: number, file: FileInfo) => acc + file.filesize, 0);
  const filesUploadingCount = filesStored.filter(({ available }: FileInfo) => !available).length;
  const activeContractsCount = contracts?.activecontracts?.length ?? 0;

  return (
    <Box sx={{ display: "grid", mb: 4, gridTemplateColumns: "repeat(4, 1fr)", gap: 4 }}>
      <DashCard>
        <DashCardTitle sx={{ fontSize: 0, color: "primary" }}>Hosts</DashCardTitle>
        <Styled.h1>{activeContractsCount}</Styled.h1>

        <Tooltip title="This is how many hosts are storing your fragmented, encrypted data. This number is usually around 50, but can fluctuate.">
          <InfoOutlinedIcon fontSize="small" sx={{ position: "absolute", top: "5px", right: "5px", opacity: 0.5 }} />
        </Tooltip>
      </DashCard>
      <DashCard>
        <DashCardTitle sx={{ fontSize: 0, color: "primary" }}>Data Stored</DashCardTitle>
        <Styled.h1>{filesize(filesStoredSize, { base: 10, round: 0 })}</Styled.h1>
      </DashCard>
      <DashCard>
        <DashCardTitle sx={{ fontSize: 0, color: "primary" }}>Files Stored</DashCardTitle>
        <Styled.h1>{formatNumber(filesStoredCount)}</Styled.h1>
      </DashCard>
      <DashCard>
        <DashCardTitle sx={{ fontSize: 0, color: "primary" }}>Files Uploading</DashCardTitle>
        <Styled.h1>{filesUploadingCount}</Styled.h1>
      </DashCard>
    </Box>
  );
};
