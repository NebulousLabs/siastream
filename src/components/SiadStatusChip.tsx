import { Chip, CircularProgress } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Done, Error } from "@material-ui/icons";
import ms from "ms";
import React from "react";
import { useConsensus, useRenterRecoveryScan, useWallet } from "../api";
import { calculateConsensusSyncProgress } from "../services/siad";

export const useChipStyles = makeStyles((theme: Theme) => ({
  error: {
    backgroundColor: theme.palette.error.main,
    color: theme.palette.error.contrastText,
  },
}));

const Spinner = () => <CircularProgress size={16} />;

export default function SiadStatusChip(props) {
  const { data: consensus, error: errorConsensus } = useConsensus({ refreshInterval: ms("5 seconds") });
  const { data: wallet, error: errorWallet } = useWallet({ refreshInterval: ms("5 seconds") });
  const { data: recoveryscan } = useRenterRecoveryScan({ refreshInterval: ms("5 seconds") });
  const chipClasses = useChipStyles();

  if (errorConsensus || errorWallet) {
    return <Chip label={"Connection interrupted"} className={chipClasses.error} icon={<Error />} {...props} />;
  }

  if (!wallet || !consensus) {
    return <Chip label={"Loading modules"} icon={<Spinner />} {...props} />;
  }

  if (!consensus.synced) {
    return <Chip label={`${calculateConsensusSyncProgress(consensus)}% Synced`} icon={<Spinner />} {...props} />;
  }

  if (wallet.rescanning) {
    return <Chip label={"Rescanning wallet"} icon={<Spinner />} {...props} />;
  }

  if (recoveryscan?.scaninprogress) {
    return <Chip label={"Contract recovery in progress"} icon={<Spinner />} {...props} />;
  }

  return <Chip label={"Synced"} color="primary" icon={<Done />} {...props} />;
}
