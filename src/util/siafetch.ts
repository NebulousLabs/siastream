import axios, { AxiosError } from "axios";

export const siafetch = {
  // TODO add abort signal capability
  get: (url) => () => axios.get(`/siad/${url}`).then(({ data }) => data),
  // TODO add abort signal capability
  post: (url) => (data) =>
    axios
      .post(`/siad/${url}`, data)
      .then(({ data }) => data)
      .catch((err: AxiosError) => {
        throw new Error(err.response.data.message);
      }),
};
export const SiaGetConsensus = siafetch.get("consensus");
export const SiaGetGateway = siafetch.get("gateway");
export const SiaGetVersion = siafetch.get("version");
export const SiaGetWallet = siafetch.get("wallet");
export const SiaGetRenter = siafetch.get("renter");
export const SiaHealthcheck = siafetch.get("healthcheck");
export const SiaInitWallet = siafetch.post("wallet/init");
export const SiaUnlockWallet = siafetch.post("wallet/unlock");
export const SiaGenerateAddress = siafetch.get("wallet/address");
export const SiaGetContracts = siafetch.get("renter/contracts");
export const SiaRestoreWallet = siafetch.post("wallet/init/seed");
export const SiaRecoveryScan = siafetch.get("renter/recoveryscan");
export const SiaGetConstants = siafetch.get("daemon/constants");
export const SiaGetFiles = siafetch.get("renter/files");
export const SiaGetFuse = siafetch.get("fuse");
export const SiaMountFuse = siafetch.get("fuse/mount");
export const SiaUnmountFuse = siafetch.get("fuse/unmount");
