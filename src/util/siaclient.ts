import { Client } from "sia-typescript";
import { ClientConfig } from "sia-typescript/build/main/lib/proto";

export const createSiaClient = (config: ClientConfig = {}) => new Client(config);
