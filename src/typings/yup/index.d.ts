import { StringSchema as YupStringSchema } from "yup";

declare module "yup" {
  interface StringSchema {
    validDir(message?: string): YupStringSchema;
    validBaseDir(message?: string): YupStringSchema;
    validSiaPath(message?: string): YupStringSchema;
  }
}
